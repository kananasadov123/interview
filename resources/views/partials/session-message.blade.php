<div class="w-50">
    @if(Session::has('success-message'))
        <div class="alert bg-success text-white alert-styled-left alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
            {{ Session::get('success-message') }}
        </div>
    @endif
    @if ($errors->any())
        <ul class="alert bg-danger text-white alert-styled-left alert-dismissible">
            @foreach ($errors->all() as $error)
                <li>
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    @endif
    @if(Session::has('success-error'))
        <div class="alert bg-danger text-white alert-styled-left alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
            {{ Session::get('success-error') }}
        </div>
    @endif
</div>
