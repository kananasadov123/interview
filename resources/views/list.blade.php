@extends('layouts.app')

@section('content')
    <div class="container list-post">
        <div class="row">
            <div class="col-md-12">
                <div class="post-header">
                    <h4>{{ __('Dashboard') }}</h4>
                    <a href="{{ route('add') }}" class="btn btn-primary">Add post</a>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Title</th>
                                <th scope="col">Description</th>
                                <th scope="col">Date</th>
                                <th scope="col">Pass</th>
                                <th scope="col">Operation</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($posts))
                                @foreach($posts as $post)
                                    @php $generateCode = Str::random(10); @endphp
                                    <tr class="list-post-table-tr" data-unique-code="{{ $generateCode }}">
                                        <td>{{ $post->title }}</td>
                                        <td>{!! $post->description !!}</td>
                                        <td>{{ $post->date }}</td>
                                        <td>{{ Carbon\Carbon::parse($post->date)->diffForhumans() }}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-outline- dropdown-toggle" type="button"
                                                        id="dropdownMenuButton" data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false">
                                                    Operation
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item"
                                                       href="{{ route('view',$post->id) }}">View</a>
                                                    <a data-unique-code="{{ $generateCode }}"
                                                       data-id="{{ $post->id }}" onclick="deletePost(this);"
                                                       class="dropdown-item"
                                                       href="#">Delete</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>Not found data</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
