@extends('layouts.app')

@section('content')
    <div class="container add-post">
        <div class="row">
            <div class="col-md-12">
                <div class="post-header">
                    <h4>Add post</h4>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('partials.session-message')
                <form action="{{ route('store') }}" method="POST" enctype="multipart/form-data"
                      class="add-post-form">
                    @csrf
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input name="title" value="{{ old('title') }}" type="text" class="form-control" id="title"
                               placeholder="Enter title">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" class="form-control" id="description"
                                  placeholder="Enter description">{{ old('description') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="date">Date</label>
                        <input max="{{ date('Y-m-d') }}" name="date" value="{{ old('date') }}" type="date"
                               class="form-control" id="date"
                               placeholder="Enter date">
                    </div>
                    <button type="submit" class="btn btn-primary">Add post</button>
                    <a href="{{ route('list') }}" class="btn btn-primary">Back</a>

                </form>
            </div>
        </div>
@endsection
