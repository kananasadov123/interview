@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Home page</div>
                    <div class="card-body">
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <a href="{{ route('list') }}" class="card-link">List post</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
