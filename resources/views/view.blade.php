@extends('layouts.app')

@section('content')
    <div class="container list-post">
        <div class="row">
            <div class="col-md-12">
                <div class="post-header">
                    <h4>View post</h4>
                    <a href="{{ route('list') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <ul class="list-group">
                    <li class="list-group-item">Title : {{ $post->title??'' }}</li>
                    <li class="list-group-item">Description : {!! $post->description??'' !!}</li>
                    <li class="list-group-item">Date : {{ $post->date??'' }}</li>
                    <li class="list-group-item">Pass date : {{ $post->passDateGeneral??'' }}</li>
                    <li class="list-group-item">Pass spesific day : {{ $post->passDateDate??'' }} days</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
