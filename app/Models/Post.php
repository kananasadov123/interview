<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //or we can use  protected $guarded = [];
    protected $fillable = ['title', 'description', 'date'];

}
