<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddPostRequest;
use App\Http\Requests\DeletePostRequest;
use App\Models\Post;
use Carbon\Carbon;

class PostsController extends Controller
{
    public function list()
    {
        $posts = Post::all();

        return view('list', compact('posts'));
    }

    public function add()
    {
        return view('add');
    }

    public function store(AddPostRequest $request)
    {
        $request->validated();

        $post = new Post();
        $post->title = $request->input('title');
        $post->description = $request->input('description');
        $post->date = $request->input('date');
        $post->save();

        return redirect()->route('list');
    }

    public function view($id)
    {
        $post = Post::find($id);

        $post->passDateDate = Carbon::now()->diffInDays(Carbon::parse($post->date));

        $post->passDateGeneral = Carbon::parse($post->date)->diffForhumans();

        return view('view', compact('post'));
    }

    public function delete(DeletePostRequest $request, $id)
    {
        $request->validated();

        $post = Post::find($id);

        if (!$post) {
            BaseController::jsonResponse(0, [], 'Post not found', 404);
        }

        $post->delete();

        return BaseController::jsonResponse(1, [], 'Post deleted', 200);
    }

}
