<?php

namespace App\Http\Controllers;

class BaseController extends Controller
{

    //usage - BaseController::jsonResponse(1, [], 'success', 200);
    public static function jsonResponse($status, $data, $message, $statusCode)
    {
        return response()->json([
            'status' => $status,
            'data' => $data,
            'message' => $message,
        ], $statusCode);
    }

    //usage - BaseController::fileUpload($clinic, $request->file("image"));
    public static function fileUpload($module, $file)
    {
        $filename = $file->store($module->getTable(), 'public');
        return $filename;
    }
}
