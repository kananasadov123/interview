<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'date' => 'required|date|date_format:Y-m-d|before:' . date('Y-m-d')
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title must filled',
            'description.required' => 'Description must filled',
            'date.required' => 'Date must filled',
        ];
    }
}
