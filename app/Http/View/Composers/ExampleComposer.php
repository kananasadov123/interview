<?php

namespace App\Http\View\Composers;

use App\Models\Permission;
use Illuminate\View\View;

class ExampleComposer
{
    public function compose(View $view)
    {
        $example = 'data';
        $view->with('example', $example);
    }
}
