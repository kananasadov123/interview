<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostsController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', [HomeController::class, 'welcome']);
    Route::get('/home', [HomeController::class, 'home'])->name('home');
    Route::get('/list', [PostsController::class, 'list'])->name('list');
    Route::get('/add', [PostsController::class, 'add'])->name('add');
    Route::post('/store', [PostsController::class, 'store'])->name('store');
    Route::get('/view/{id}', [PostsController::class, 'view'])->name('view');
    Route::delete('/delete/{id}', [PostsController::class, 'delete'])->name('delete');
});
