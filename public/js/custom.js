const token = document.head.querySelector('meta[name="csrf-token"]').getAttribute('content');

function deletePost(object) {
    let id = object.getAttribute('data-id');
    let uniqueCode = object.getAttribute('data-unique-code');
    let elementTr = document.querySelector(`.list-post-table-tr[data-unique-code="${uniqueCode}"]`);

    fetch('/delete/' + id, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            "X-CSRF-TOKEN'": token
        },
        body: JSON.stringify(
            {
                id: id,
                _token: token
            }),
    })
        .then(response => response.json())
        .then(data => {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: data.message,
                timer: 1500
            });
            elementTr.remove();
        })
        .catch((error) => {
            Swal.fire({
                position: 'center',
                icon: 'warning',
                title: 'Something go wrong',
                timer: 1500
            });
        });

}
